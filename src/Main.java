import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        // runFull();
        runTest();
    }

    private static void runTest() throws Exception {
        String imagePath = "image.jpg";
        String newImagePath = "imageCawfjfefdd.jpg";
        int thread = 6;
        int newWidth = 790;
        int newHeight = 630;
        Instant starts = Instant.now();
        SeamCarver carver = new SeamCarver(imagePath, newWidth, newHeight, thread);
        carver.carve();
        carver.saveCarvedImage(newImagePath, "jpg");
        Instant ends = Instant.now();
        System.out.println((Duration.between(starts, ends).toMillis()/(float)1000) + " s");
    }

    private static void runFull() throws Exception {
        System.out.println("\nEnter the image's path to resize:");
        String imagePath = scanner.nextLine();
        System.out.println("\nEnter the output image's path:");
        String newImagePath = scanner.nextLine();
        System.out.println("\nEnter number of threads:");
        int thread = Integer.parseInt(scanner.nextLine());
        int newWidth = validateDimension("width");
        int newHeight = validateDimension("height");
        Instant starts = Instant.now();
        SeamCarver carver = new SeamCarver(imagePath, newWidth, newHeight, thread);
        carver.carve();
        carver.saveCarvedImage(newImagePath, "jpg");
        Instant ends = Instant.now();
        System.out.println((Duration.between(starts, ends).toMillis()/(float)1000) + " s");
    }

    public static int validateDimension(String dim) {
        int number;
        System.out.println("\nEnter new " + dim + ":");
        do {
            try {
                number = Integer.parseInt(scanner.nextLine());
                if (number < 1) {
                    System.out.println("Invalid argument, input at least 1");
                }
            } catch (NumberFormatException e) {
                number = 0;
                System.out.println("Invalid argument, input at least 1");
            }
        } while (number < 1);
        return number;
    }
}
