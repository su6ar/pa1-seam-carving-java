public class Seam {
    private double energy;
    private int[] pixels;
    private String direction;

    public Seam(int s, String dir) {
        pixels = new int[s];
        direction = dir;
    }

    String getDirection() {
        return direction;
    }

    int[] getPixels() {
        return pixels;
    }

    void setPixels(int position, int value) {
        pixels[position] = value;
    }

    double getEnergy() {
        return energy;
    }

    void setEnergy(double energy) {
        this.energy = energy;
    }
}