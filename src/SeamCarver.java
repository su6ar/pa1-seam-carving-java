import java.awt.*;
import java.awt.desktop.SystemEventListener;
import java.io.IOException;
import java.io.File;
import java.awt.image.WritableRaster;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.util.ArrayList;
import javax.imageio.ImageIO;


public class SeamCarver {
    private BufferedImage inputImage;
    private BufferedImage carvedImage;
    private ArrayList<Seam> seams;
    private int width;
    private int height;
    private int threadN;
    private char dim;

    public SeamCarver(String inputPath, int width, int height, int threadN) throws IOException {
        this.carvedImage = null;
        this.width = width;
        this.height = height;
        this.threadN = threadN;
        try {
            this.inputImage = ImageIO.read(this.getClass().getResource(inputPath));
            this.carvedImage = copyImage(inputImage);
        } catch (IOException e) {
            System.out.println("Input file could not be opened: " + inputPath);
            throw (e);
        }
        if (inputImage.getWidth() >= inputImage.getHeight())
        {
            this.dim = 'w';
        }
        else
        {
            this.dim = 'h';
        }
        this.seams = new ArrayList<>();
    }

    public void carve() throws IllegalArgumentException, InterruptedException {
        int numCarveHorizontal = inputImage.getHeight() - height;
        int numCarveVertical = inputImage.getWidth() - width;
        if (numCarveHorizontal < 0 || numCarveHorizontal >= inputImage.getWidth() || numCarveVertical < 0 || numCarveVertical >= inputImage.getHeight()) {
            System.out.println("Width and height of the output image should be less than or equal to the original!");
            throw new IllegalArgumentException();
        }
        while (numCarveHorizontal > 0 || numCarveVertical > 0) {
            Seam horizontalSeam;
            Seam verticalSeam;
            double[][] energyTable = calculateEnergyTable(carvedImage);
            if (numCarveHorizontal > 0 && numCarveVertical > 0) {
                horizontalSeam = getHorizontalSeam(energyTable);
                verticalSeam = getVerticalSeam(energyTable);
                if (horizontalSeam.getEnergy() < verticalSeam.getEnergy()) {
                    seams.add(horizontalSeam);
                    removeSeam(horizontalSeam);
                    numCarveHorizontal--;
                } else {
                    seams.add(verticalSeam);
                    removeSeam(verticalSeam);
                    numCarveVertical--;
                }
            }
            else if (numCarveHorizontal > 0) {
                horizontalSeam = getHorizontalSeam(energyTable);
                seams.add(horizontalSeam);
                removeSeam(horizontalSeam);
                numCarveHorizontal--;
            }
            else {
                verticalSeam = getVerticalSeam(energyTable);
                seams.add(verticalSeam);
                removeSeam(verticalSeam);
                numCarveVertical--;
            }
        }
    }

    private double[][] calculateEnergyTable(BufferedImage image) throws InterruptedException {
        int part;
        int width = image.getWidth();
        int height = image.getHeight();
        double[][] energyTable = new double[width][height];

        Thread[] threads = new Thread[this.threadN];
        if (this.dim == 'w')
        {
            part = width / this.threadN;
        }
        else
        {
            part = height / this.threadN;
        }

        for (int i = 0; i < threads.length; i++)
        {
            int threadIndex = i;
            threads[i] = new Thread(() -> { GenerateEnergy(threadIndex, part, energyTable, width, height, image); });
            threads[i].start();
        }

        for (Thread thread : threads) {
            thread.join();
        }
        return energyTable;
    }

    private void GenerateEnergy(int threadIndex, int part, double[][] energyTable, int width, int height, BufferedImage image)
    {
        int end = (threadIndex + 1) * part;
        if (dim == 'w')
        {
            ProcessEnergy(energyTable, threadIndex * part, 0, threadIndex == threadN - 1 ? end + (image.getWidth() - end) : end, height, width, height, image);
        }
        else
        {
            ProcessEnergy(energyTable, 0, threadIndex * part, width, threadIndex == threadN - 1 ? end + (image.getHeight() - end) : end, width, height, image);
        }
    }

    private void ProcessEnergy(double[][] energyTable, int x, int y, int endx, int endy, int width, int height, BufferedImage image)
    {
        for (int i = x; i < endx; i++)
        {
            for (int j = y; j < endy; j++)
            {
                double xEnergy, yEnergy, totalEnergy;

                int xPrevRGB = image.getRGB((i - 1 + endx) % endx, j);
                int xNextRGB = image.getRGB((i + 1 + endx) % endx, j);
                xEnergy = getEnergy(xPrevRGB, xNextRGB);
                int yPrevRGB = image.getRGB(i, (j - 1 + endy) % endy);
                int yNextRGB = image.getRGB(i, (j + 1 + endy) % endy);
                yEnergy = getEnergy(yPrevRGB, yNextRGB);
                totalEnergy = xEnergy + yEnergy;
                energyTable[i][j] = totalEnergy;
            }
        }
    }

    private double getEnergy(int rgb1, int rgb2) {
        double b1 = (rgb1) & 0xff;
        double g1 = (rgb1 >> 8) & 0xff;
        double r1 = (rgb1 >> 16) & 0xff;
        double b2 = (rgb2) & 0xff;
        double g2 = (rgb2 >> 8) & 0xff;
        double r2 = (rgb2 >> 16) & 0xff;
        return (r1 - r2) * (r1 - r2) + (g1 - g2) * (g1 - g2) + (b1 - b2) * (b1 - b2);
    }

    private Seam getHorizontalSeam(double[][] energyTable) {
        int width = energyTable.length;
        int height = energyTable[0].length;
        Seam seam = new Seam(width, "horizontal");
        double[][] horizontalDp = new double[width][height];
        int[][] prev = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                double minValue;
                if (i == 0) {
                    horizontalDp[i][j] = energyTable[i][j];
                    prev[i][j] = -1;
                    continue;
                }
                else if (j == 0) {
                    minValue = Math.min(horizontalDp[i - 1][j], horizontalDp[i - 1][j + 1]);
                    if (minValue == horizontalDp[i - 1][j]) {
                        prev[i][j] = j;
                    } else {
                        prev[i][j] = j + 1;
                    }
                }
                else if (j == height - 1) {
                    minValue = Math.min(horizontalDp[i - 1][j], horizontalDp[i - 1][j - 1]);
                    if (minValue == horizontalDp[i - 1][j]) {
                        prev[i][j] = j;
                    } else {
                        prev[i][j] = j - 1;
                    }
                }
                else {
                    minValue = Math.min(horizontalDp[i - 1][j], Math.min(horizontalDp[i - 1][j - 1], horizontalDp[i - 1][j + 1]));
                    if (minValue == horizontalDp[i - 1][j]) {
                        prev[i][j] = j;
                    } else if (minValue == horizontalDp[i - 1][j - 1]) {
                        prev[i][j] = j - 1;
                    } else {
                        prev[i][j] = j + 1;
                    }
                }
                horizontalDp[i][j] = energyTable[i][j] + minValue;
            }
        }
        double minEnergy = horizontalDp[width - 1][0];
        int minCoord = 0;
        for (int j = 0; j < height; j++) {
            if (minEnergy > horizontalDp[width - 1][j]) {
                minEnergy = horizontalDp[width - 1][j];
                minCoord = j;
            }
        }
        seam.setEnergy(minEnergy);
        for (int i = width - 1; i >= 0; i--) {
            seam.setPixels(i, minCoord);
            minCoord = prev[i][minCoord];
        }
        return seam;
    }

    private Seam getVerticalSeam(double[][] energyTable) {
        int width = energyTable.length;
        int height = energyTable[0].length;
        Seam seam = new Seam(height, "vertical");
        double[][] verticalDp = new double[width][height];
        int[][] prev = new int[width][height];
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                double minValue;
                if (j == 0) {
                    verticalDp[i][j] = energyTable[i][j];
                    prev[i][j] = -1;
                    continue;
                }
                else if (i == 0) {
                    minValue = Math.min(verticalDp[i][j - 1], verticalDp[i + 1][j - 1]);
                    if (minValue == verticalDp[i][j - 1]) {
                        prev[i][j] = i;
                    } else {
                        prev[i][j] = i + 1;
                    }
                }
                else if (i == width - 1) {
                    minValue = Math.min(verticalDp[i][j - 1], verticalDp[i - 1][j - 1]);
                    if (minValue == verticalDp[i][j - 1]) {
                        prev[i][j] = i;
                    } else {
                        prev[i][j] = i - 1;
                    }
                }
                else {
                    minValue = Math.min(verticalDp[i][j - 1], Math.min(verticalDp[i - 1][j - 1], verticalDp[i + 1][j - 1]));
                    if (minValue == verticalDp[i][j - 1]) {
                        prev[i][j] = i;
                    } else if (minValue == verticalDp[i - 1][j - 1]) {
                        prev[i][j] = i - 1;
                    } else {
                        prev[i][j] = i + 1;
                    }
                }
                verticalDp[i][j] = energyTable[i][j] + minValue;
            }
        }
        double minEnergy = verticalDp[0][height - 1];
        int minCoord = 0;
        for (int i = 0; i < width; i++) {
            if (minEnergy > verticalDp[i][height - 1]) {
                minEnergy = verticalDp[i][height - 1];
                minCoord = i;
            }
        }
        seam.setEnergy(minEnergy);
        for (int j = height - 1; j >= 0; j--) {
            seam.setPixels(j, minCoord);
            minCoord = prev[minCoord][j];
        }
        return seam;
    }

    private void removeSeam(Seam seam) {
        int width = carvedImage.getWidth();
        int height = carvedImage.getHeight();
        BufferedImage imageNew;
        if (seam.getDirection().equals("horizontal")) {
            imageNew = new BufferedImage(width, height - 1, BufferedImage.TYPE_INT_RGB);
            for (int i = 0; i < width; i++) {
                boolean moveToNext = false;
                for (int j = 0; j < height - 1; j++) {
                    if (seam.getPixels()[i] == j) {
                        moveToNext = true;
                    }
                    if (moveToNext)
                        imageNew.setRGB(i, j, carvedImage.getRGB(i, j + 1));
                    else
                        imageNew.setRGB(i, j, carvedImage.getRGB(i, j));
                }
            }
        } else {
            imageNew = new BufferedImage(width - 1, height, BufferedImage.TYPE_INT_RGB);
            for (int j = 0; j < height; j++) {
                boolean moveToNext = false;
                for (int i = 0; i < width - 1; i++) {
                    if (seam.getPixels()[j] == i) {
                        moveToNext = true;
                    }
                    if (moveToNext) {
                        imageNew.setRGB(i, j, carvedImage.getRGB(i + 1, j));
                    } else {
                        imageNew.setRGB(i, j, carvedImage.getRGB(i, j));
                    }
                }
            }
        }
        carvedImage = imageNew;
    }

    public void saveCarvedImage(String filePath, String type) throws IOException {
        try {
            File outputFile = new File(filePath);
            ImageIO.write(carvedImage, type, outputFile);
            System.out.println("\nWriting completed.");
        } catch (IOException e) {
            System.out.println("Cannot open output file: " + filePath);
            throw (e);
        }
    }

    private BufferedImage copyImage(BufferedImage img) {
        ColorModel colorModel = img.getColorModel();
        boolean isAlphaPremultiplied = colorModel.isAlphaPremultiplied();
        WritableRaster writableRaster = img.copyData(null);
        return new BufferedImage(colorModel, writableRaster, isAlphaPremultiplied, null);
    }
}